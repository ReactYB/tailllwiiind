import FooterOne from "./components/footers/FooterOne";
import HeadersOne from "./components/headers/HeadersOne";
import News from "./components/news/News";
import SidbarOne from "./components/sidebar/SidbarOne";

function App() {
  return (
    <>
      <HeadersOne />
      <div className="flex flex-col lg:flex-row">
      <SidbarOne />
      <div className="mx-auto w-full max-w-7xl">
        <News/>
      </div></div>
      <FooterOne />
    </>
  );
}

export default App;
