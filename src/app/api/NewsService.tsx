import axios, { AxiosResponse } from "axios";
import { toast } from "react-toastify";
import { historyIndex } from "../../index";
//https://github.com/SauravKanchan/NewsAPI
// Simulation d'une reponse lente
const sleep = (ms: number) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};
const NewsInstance = axios.create({
  baseURL: "https://saurav.tech/NewsAPI",
});

// Intersepteur de response
NewsInstance.interceptors.response.use(
  async (response :any) => {
    await sleep(2000);
    return response;
  },
  async (error: any) => {
    await sleep(2000);
    const { status, data, config } = error.response!;
    switch (status) {
      case 400:
        if (typeof data === "string") toast.error(data);
        if (config.method === "get" && data.errors.hasOwnProperty("id"))
          historyIndex.push("/not-found");
        if (data.errors) {
          const modalStateErrors = [];
          for (const key in data.errors) {
            if (data.errors[key]) {
              modalStateErrors.push(data.errors[key]);
            }
          }
          throw modalStateErrors.flat();
        }
        break;
      case 401:
        toast.error("Unauthorized");
        break;
      case 404:
        toast.error("Not Found");
        historyIndex.push("/404");
        break;
      case 500:
        historyIndex.push("/server-error");
        break;
    }
    return Promise.reject(error);
  }
);

// Extraise Data de la Response
const responseBody = <T,>(response: AxiosResponse<T>) => response.data;

// List des requests
const requests = {
  get: <T,>(url: string) => NewsInstance.get<T>(url).then(responseBody),
  post: <T,>(url: string, body: {}) =>
    NewsInstance.post<T>(url, body).then(responseBody),
  put: <T,>(url: string, body: {}) =>
    NewsInstance.put<T>(url, body).then(responseBody),
  delete: <T,>(url: string) => NewsInstance.delete<T>(url).then(responseBody),
};

// News Calls
const getNews = {
  list: () => requests.get<any>("/sources.json"), // List de tous les donnees NEWS
  category: (category: string) =>
    requests.get<any>(`/top-headlines/category/` + category + `/us.json`), // List de tous les donnees NEWS par categories
  pays: (country: string) =>
    requests.get<any>(`/top-headlines/category/general/` + country + `.json`), // List de tous les donnees NEWS par pays
  channel: (channel: string) =>
    requests.get<any>(`/everything/` + channel + `.json`), // List de tous les donnees NEWS par channel
  details: (id: string) => requests.get<any>(`/News/${id}`), // List de tous les donnees NEWS par id ?
};

const Agent = { getNews };

export default Agent;