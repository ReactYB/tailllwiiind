import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { RootState } from "../../store";
import Agent from "../../api/NewsService";

export interface NewsState {
  news:[] ;
  loading: boolean;
  status: "idle" | "loading" | "failed";
  error:string | undefined
}

const initialState: NewsState = {
  news: [],
  status: "idle",
  loading: false,
  error: "",
};

export const fetchNews = createAsyncThunk('NewsData',()=>{
return Agent.getNews.list()}) 

const newsSlice = createSlice({
  name: "news",
  initialState,
  reducers: {
    removeTodo: (state, action) => {
    //   console.log(state.news);

      const { id } = action.payload;
      console.log(id);

    //   state.news = state.news.filter((item) => item.id !== id);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchNews.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchNews.fulfilled, (state, action) => {
      state.loading = false;
      state.news = action.payload.sources;
    });
    builder.addCase(fetchNews.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });
  },
});
export const selectCount = (state: RootState) => state.news.news;
export const { removeTodo } = newsSlice.actions;
export default newsSlice.reducer