const RoundedUser = () => {
  return (
    <div className="flex items-center justify-center min-h-screen bg-white">
      <div className="flex flex-row space-x-8">
        <button className="max-w-xs rounded-full flex items-center text-sm focus:outline-none relative">
          <img
            className="h-16 w-16 rounded-full"
            src="https://i.pravatar.cc/32"
            alt="avatar"
          />
          <span className="absolute bottom-0 right-0 h-4 w-4 rounded-full bg-emerald-500 ring ring-white"></span>
        </button>
        <button className="max-w-xs rounded-full flex items-center text-sm focus:outline-none relative">
          <img
            className="h-16 w-16 rounded-full"
            src="https://i.pravatar.cc/32"
            alt="avatar"
          />
          <span className="absolute bottom-0 right-0 h-4 w-4 rounded-full bg-yellow-500 ring ring-white"></span>
        </button>
        <button className="max-w-xs rounded-full flex items-center text-sm focus:outline-none relative">
          <img
            className="h-16 w-16 rounded-full"
            src="https://i.pravatar.cc/32"
            alt="avatar"
          />
          <span className="absolute bottom-0 right-0 h-4 w-4 rounded-full bg-red-500 ring ring-white"></span>
        </button>
        <button className="max-w-xs rounded-full flex items-center text-sm focus:outline-none relative">
          <img
            className="h-16 w-16 rounded-full"
            src="https://i.pravatar.cc/32"
            alt="avatar"
          />
          <span className="absolute bottom-0 right-0 h-4 w-4 rounded-full bg-white border-4 border-gray-500 ring ring-white"></span>
        </button>
      </div>
    </div>
  );
}

export default RoundedUser