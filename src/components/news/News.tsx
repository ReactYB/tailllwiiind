import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../../app/store";
import { fetchNews,removeTodo } from "../../app/states/News/NewsSlice";
import UserCardArticle from "../cards/UserCardArticle";
import LoadingOne from "../loading/LoadingOne";

const News = () => {
  const dispatch = useDispatch<AppDispatch>();
  const news = useSelector((state: RootState) => state.news);
  
  useEffect(() => {
    if (news.news.length <= 1) {
      dispatch(fetchNews());
      dispatch(removeTodo({id: 10}));
    }
  }, [dispatch, news.news.length]);

  return (
    <>
      {news.loading ? (
        <LoadingOne />
      ) : (
        <div>
          <button className="block mx-auto">click</button>
          {news.news.map((e: any) => (
            <div key={e.description} className="flex flex-col">
              <UserCardArticle news={e} />
            </div>
          ))}{" "}
        </div>
      )}
    </>
  );
};

export default News;
