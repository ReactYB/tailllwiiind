const LoadingOne = () => {
  return (
    <div className="w-full">
      <section className="relative content-center justify-center flex h-screen mt-72 gap-4">
        <div className=" bg-blue-200 w-32 h-32 absolute animate-ping rounded-full delay-5s shadow-xl"></div>
        <div className=" bg-blue-400 w-24 h-24 mt-[16px] absolute animate-ping rounded-full shadow-xl"></div>
        <div className=" bg-blue-700 w-16 h-16 absolute mt-[32px] animate-pulse rounded-full shadow-xl"></div>
      </section>
    </div>
  );
};

export default LoadingOne;
