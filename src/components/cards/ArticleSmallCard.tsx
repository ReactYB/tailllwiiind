import React from 'react'

const ArticleSmallCard = () => {
  return (
    <div className="relative flex min-h-screen flex-col justify-center bg-gradient-to-r from-rose-100 to-teal-100">
      <div className="mx-auto flex w-96 flex-col justify-center bg-white rounded-2xl shadow-xl shadow-slate-300/60">
        <img
          className="aspect-video w-96 rounded-t-2xl object-cover object-center"
          src="https://i.pravatar.cc/144"
        />
        <div className="p-4">
          <small className="text-blue-400 text-xs">Automobile company</small>
          <h1 className="text-2xl font-medium text-slate-600 pb-2">
            Dodge Car
          </h1>
          <p className="text-sm tracking-tight font-light text-slate-400 leading-6">
            Dodge is an American brand of automobiles and a division of
            Stellantis, based in Auburn Hills, Michigan..
          </p>
        </div>
      </div>
    </div>
  );
}

export default ArticleSmallCard